import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  constructor(private route: ActivatedRoute,private authorservice:AuthorsService) { }

  author; 
  id;
  newauthor:string;
  authors:any;

  authors$:Observable<any>;

  add(){
    this.authorservice.addAuthors(this.newauthor);
  }

  ngOnInit() {
    this.authors$ = this.authorservice.getAuthors();
    this.author = this.route.snapshot.params.author;
    this.id = this.route.snapshot.params.id;
  }


}
   
  


