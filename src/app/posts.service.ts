import { User } from './interface/user';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interface/post';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postapi = "https://jsonplaceholder.typicode.com/posts "
  userapi = "https://jsonplaceholder.typicode.com/users"

  constructor(private _http: HttpClient) { }

  getPost(){
    return this._http.get<Post[]>(this.postapi);
  }
  
  getUser(){
    return this._http.get<User[]>(this.userapi);
  }
}
