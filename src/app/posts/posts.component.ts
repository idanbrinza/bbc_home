import { User } from './../interface/user';
import { PostsService } from './../posts.service';
import { Post } from './../interface/post';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
 

  Posts$: Post[];
  User$: User[];

  constructor(private postservice: PostsService) { }

  ngOnInit() {
    this.postservice.getUser()
    .subscribe(data =>this.User$ = data );
    this.postservice.getPost()
    .subscribe(data =>this.Posts$ = data );
   
  }

}
