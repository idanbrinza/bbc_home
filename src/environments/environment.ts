// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production:false,
  firebaseConfig:{
    apiKey: "AIzaSyCgEQTc_1QVDvhaj3p5t7fk1nz1ouTONYc",
    authDomain: "idan-bbc.firebaseapp.com",
    databaseURL: "https://idan-bbc.firebaseio.com",
    projectId: "idan-bbc",
    storageBucket: "idan-bbc.appspot.com",
    messagingSenderId: "1049375567401",
    appId: "1:1049375567401:web:f86f7b5454bcd4307fb638",
    measurementId: "G-KEE95XXVHL"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
